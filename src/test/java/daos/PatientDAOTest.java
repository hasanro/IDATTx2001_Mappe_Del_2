package daos;

import model.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class PatientDAOTest {
    private final PatientDAO patientDAO = new PatientDAO(
            Persistence.createEntityManagerFactory("st-olavs-register-test").createEntityManager());
    private Patient testPatient;

    @BeforeEach
    void resetPatient() {
        testPatient = new Patient("Ola", "Normann", "12345678910");
    }

    @Test
    @DisplayName("Patient is created")
    @Transactional
    void testPatientIsCreated() throws Exception {
        patientDAO.create(testPatient);

        assertTrue(patientDAO.exists(testPatient));
    }

    @Test
    @DisplayName("Patient data is correct")
    @Transactional
    void testPatientDataIsCorrect() throws Exception {
        patientDAO.create(testPatient);

        Patient patient = patientDAO.getSpecificPatient(testPatient.getPersonalID()).get();

        boolean firstName = patient.getFirstName().equals(testPatient.getFirstName());
        boolean lastName = patient.getLastName().equals(testPatient.getLastName());
        boolean personalID = patient.getPersonalID().equals(testPatient.getPersonalID());

        assertTrue(firstName && lastName && personalID);
    }

    @Test
    @DisplayName("Patient deleted from database")
    @Transactional
    void testPatientDeletedFromDatabase() throws Exception {
        patientDAO.create(testPatient);
        patientDAO.delete(testPatient);

        assertFalse(patientDAO.exists(testPatient));
    }

    @Test
    @DisplayName("Create patient thrown exception")
    @Transactional
    void testCreatePatientThrownException() throws Exception {
        patientDAO.create(testPatient);

        assertThrows(SQLException.class, () -> patientDAO.create(testPatient));
    }

    @Test
    @DisplayName("Length of patient database")
    @Transactional
    void testLengthOfPatientDatabase() throws Exception {
        Patient one = (new Patient("Michael", "Corleone", "Vito Corleone", "12345678901"));
        Patient two = (new Patient("Ace", "Ventura", "Rhinoceros", "56789012345"));
        Patient three = (new Patient("Marty", "McFly", "Doc", "19851955012"));
        Patient four = (new Patient("Beatrix", "Kiddo", "Billy", "34567890123"));
        Patient five = (new Patient("Forrest ", "Gump", "Jenny", "45678901234"));
        Patient six = (new Patient("Tyler", "Durden", "Insane", "Tyler Durden", "01234567890"));

        patientDAO.create(one);
        patientDAO.create(two);
        patientDAO.create(three);
        patientDAO.create(four);
        patientDAO.create(five);
        patientDAO.create(six);
        patientDAO.create(testPatient);

        assertEquals(7, patientDAO.getAll().size());
    }
}
