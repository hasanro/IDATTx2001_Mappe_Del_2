package model;

import controllers.PatientRegisterController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;

import static model.ForretningsCode.readData;
import static model.ForretningsCode.writeToCSV;
import static org.junit.jupiter.api.Assertions.*;

public class ForretningsCodeTest {
    ArrayList<Patient> testList = new ArrayList<>();
    ObservableList<Patient> testRegister = FXCollections.observableArrayList();

    @Test
    @DisplayName("Change of patient variables success")
    public void testChangeOfPatientVariablesSuccess() {
        Patient updateTestPatient = new Patient("testMichael", "CorleoneTwo", "fine", "Vito", "11111111111");
        Patient testPatient = new Patient("Michael", "Corleone", "crazy", "Vito Corleone", "12345678901");
        testRegister.add(testPatient);
        PatientRegisterController.setPatient(testPatient);

        ForretningsCode.updatePatientInRegister(updateTestPatient, testRegister);

        boolean testFirstName = updateTestPatient.getFirstName().equals(testRegister.get(0).getFirstName());
        boolean testLastName = updateTestPatient.getLastName().equals(testRegister.get(0).getLastName());
        boolean testDiagnosis = updateTestPatient.getDiagnosis().equals(testRegister.get(0).getDiagnosis());
        boolean testGeneralPractitioner = updateTestPatient.getGeneralPractitioner()
                .equals(testRegister.get(0).getGeneralPractitioner());
        boolean testPersonalID = updateTestPatient.getPersonalID().equals(testRegister.get(0).getPersonalID());

        assertTrue(testFirstName && testLastName && testDiagnosis && testGeneralPractitioner && testPersonalID);
    }

    @Test
    @DisplayName("Check if ID to set does not belong to another patient")
    public void testCheckIfIDToSetDoesNotBelongToAnotherPatient() {
        Patient updateTestPatient = new Patient("testMichael", "CorleoneTwo", "fine", "Vito", "11111111111");
        Patient testPatient = new Patient("Michael", "Corleone", "crazy", "Vito Corleone", "12345678901");
        testRegister.add(testPatient);
        testRegister.add(updateTestPatient);
        PatientRegisterController.setPatient(testPatient);

        assertFalse(ForretningsCode.validatePatient("11111111112", testRegister));
    }

    @Test
    @DisplayName("Check if ID to set belongs to another patient")
    public void testCheckIfIDToSetBelongsToAnotherPatient() {
        Patient updateTestPatient = new Patient("testMichael", "CorleoneTwo", "fine", "Vito", "11111111111");
        Patient testPatient = new Patient("Michael", "Corleone", "crazy", "Vito Corleone", "12345678901");
        testRegister.add(testPatient);
        testRegister.add(updateTestPatient);
        PatientRegisterController.setPatient(testPatient);

        assertTrue(ForretningsCode.validatePatient(updateTestPatient.getPersonalID(), testRegister));
    }

    @Test
    @DisplayName("Patients objects added from import")
    public void testPatientObjectsAddedFromImport() throws Exception {
        File file = new File("src/test/resources/filesForTest/Patients(1)-Test.csv");

        testList.addAll(readData(file));

        assertFalse(testList.isEmpty());
    }

    @Test
    @DisplayName("Patient object exported to file")
    public void testPatientObjectsExportedToFile() throws Exception {
        File file = new File("src/test/resources/filesForTest/emty-file-to-fill-test.csv");

        testList.add(new Patient("Michael", "Corleone", "Vito Corleone", "12345678901"));
        testList.add(new Patient("Ace", "Ventura", "Rhinoceros", "56789012345"));
        testList.add(new Patient("Marty", "McFly", "Doc", "19851955012"));
        testList.add(new Patient("Beatrix", "Kiddo", "Billy", "34567890123"));
        testList.add(new Patient("Forrest ", "Gump", "Jenny", "45678901234"));
        testList.add(new Patient("Tyler", "Durden", "Insane", "Tyler Durden", "01234567890"));

        writeToCSV(file, testList);
        assertNotEquals(0, file.length());
    }

    @Test
    @DisplayName("Wrong patient content throws exception")
    public void testWrongPatientContentThrowsException() {
        File file = new File("src/test/resources/filesForTest/wrong-content-test.csv");

        assertThrows(IllegalArgumentException.class, () -> testList.addAll(readData(file)));
    }
}
