package factory;

import App.App;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Factory class to make and return {@link Stage} objects.
 */
public class PopUp {

    /**
     * Method made to give user quick feedback on Exceptions.
     * 
     * @param title
     *            title of the window.
     * @param body
     *            message to user.
     * 
     * @return returns window.
     */
    public static Dialog<ButtonType> exceptionDialog(String title, String body) {
        Dialog<ButtonType> confirmDialog = new Dialog<>();
        confirmDialog.setTitle(title);
        confirmDialog.setContentText(body);
        confirmDialog.getDialogPane().getButtonTypes().add(ButtonType.OK);

        Stage stage = (Stage) confirmDialog.getDialogPane().getScene().getWindow();
        stage.setTitle("Failed!");
        stage.getIcons().add(new Image(String.valueOf(App.class.getResource("/images/exclamation.png"))));

        return confirmDialog;
    }

    /**
     * Makes popup window for the wanted fxml file and sets the window.
     * 
     * @param fxml
     *            fxml file to make window for.
     * 
     * @return window stage.
     * 
     * @throws IOException
     *             if FXML Loader fails.
     */
    public static Stage getPopUp(String fxml) throws IOException {
        Stage window = new Stage();
        FXMLLoader loader = FXMLFile.fXMLLoader(fxml);

        window.setScene(new Scene(loader.load()));
        window.setResizable(false);
        if (fxml.equalsIgnoreCase("patient-details")) {
            window.getIcons().add(new Image(String.valueOf(App.class.getResource("/images/details-patient.png"))));
        } else if (fxml.equalsIgnoreCase("info")) {
            window.setTitle("About - Info");
            window.getIcons().add(new Image(String.valueOf(App.class.getResource("/images/info.png"))));
        }
        return window;
    }
}
