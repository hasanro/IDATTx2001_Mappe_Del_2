package factory;

import App.App;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * Factory for {@link FXMLLoader} objects to load stages.
 */
public class FXMLFile {
    private FXMLFile() {
    }

    /**
     * Method loads the parent fxml file.
     *
     * @param fxml
     *            name of fxml file.
     *
     * @return parent scene.
     *
     * @throws IOException
     *             exception thrown if FXMLLoader or path fails.
     */
    public static Parent loadFXML(String fxml) throws IOException {
        String path = String.format("/fxml/%s.fxml", fxml);
        FXMLLoader fxmlLoader = new FXMLLoader(FXMLFile.class.getResource(path));
        return fxmlLoader.load();
    }

    /**
     * Makes FXML for the given FXML file.
     * 
     * @param fxml
     *            name of fxml file to be loaded.
     * 
     * @return FXMLLoader
     */
    public static FXMLLoader fXMLLoader(String fxml) {
        String path = String.format("/fxml/%s.fxml", fxml);
        return new FXMLLoader(App.class.getResource(path));
    }

}
