package daos;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Interface for DAO(Data access objects)
 * 
 * @param <T>
 *            Entity type the DAO interacts with
 */
public interface DAO<T> {

    /**
     * Retrieves an object with given iD.
     * 
     * @param iD
     *            ID of object.
     * 
     * @return T.
     * 
     * @throws SQLException
     *             if query fails.
     */
    Optional<T> getSpecificPatient(String iD) throws SQLException;

    /**
     * Retrieves all objects from database
     * 
     * @return T.
     * 
     * @throws SQLException
     *             if query fails.
     */
    List<T> getAll() throws SQLException;

    /**
     * Create instance of object T in database.
     * 
     * @param entity
     *            object to create.
     * 
     * @throws SQLException
     *             if query fails.
     */
    void create(T entity) throws SQLException;

    /**
     * Deletes given object database.
     * 
     * @param entity
     *            object to delete.
     * 
     * @throws SQLException
     *             if query fails.
     */
    void delete(T entity) throws SQLException;

    /**
     * Checks if object exists.
     * 
     * @param entity
     *            object to check.
     * 
     * @return true if object exists, else false.
     * 
     * @throws SQLException
     *             if query fails.
     */
    boolean exists(T entity) throws SQLException;
}
