package daos;

import model.Patient;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * DAO(Data access object) for {@link Patient}
 */
public class PatientDAO implements DAO<Patient> {

    private EntityManager entityManager;

    /**
     * Constuctor of the class, creates a new entity manager.
     * 
     * @param entityManager
     *            Persistence entity manager.
     */
    public PatientDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Retrieves Patient with given personal ID.
     * 
     * @param iD
     *            ID of patient.
     * 
     * @return patient.
     *
     */
    @Override
    public Optional<Patient> getSpecificPatient(String iD) {
        try {
            Query query = entityManager.createQuery("SELECT c FROM Patient c WHERE c.personalID LIKE :iD")
                    .setParameter("iD", iD);
            return Optional.of(Objects.requireNonNull((Patient) query.getSingleResult()));
        } catch (NullPointerException e) {
            return Optional.empty();
        } catch (Exception e) {
            throw new IllegalArgumentException("Patient with Social Security Number: " + iD + " does not exist");
        }
    }

    /**
     * Makes list of DAO.
     * 
     * @return list of DAO.
     * 
     * @throws SQLException
     *             if query fails.
     */
    @Override
    public List<Patient> getAll() throws SQLException {
        Query query = entityManager.createQuery("SELECT c FROM Patient c");
        return query.getResultList();
    }

    /**
     * Creates a patient object if it does not already exist.
     * 
     * @param patient
     *            patient to create.
     * 
     * @throws SQLException
     *             if query fails.
     * @throws EntityExistsException
     *             if entity exists.
     */
    @Override
    public void create(Patient patient) throws SQLException, EntityExistsException {
        EntityTransaction transaction = null;
        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            if (exists(patient)) {
                throw new EntityExistsException("Patient already exists.");
            } else {
                entityManager.persist(patient);
            }
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new SQLException(patient.getFirstName() + ", " + patient.getPersonalID(), e.getMessage());
        }
    }

    /**
     * Deleted the given patient from database.
     * 
     * @param patient
     *            patient to delete.
     * 
     * @throws SQLException
     *             if query fails.
     */
    @Override
    public void delete(Patient patient) throws SQLException {
        EntityTransaction transaction = null;

        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.remove(patient);
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new SQLException(patient.getFirstName() + ", " + patient.getPersonalID(), e.getMessage());
        }
    }

    /**
     * Checks if patient exists in database
     * 
     * @param patient
     *            patient to check.
     * 
     * @return returns true if patient exists.
     * 
     * @throws SQLException
     *             if query fails.
     */
    @Override
    public boolean exists(Patient patient) throws SQLException {
        return getAll().contains(patient);
    }
}
