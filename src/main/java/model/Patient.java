package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class to make Patient object.
 */
@Entity
public class Patient implements Serializable {
    @Id
    private String personalID;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;

    private String diagnosis;

    private String generalPractitioner;

    /**
     * Constructor of the class.
     * 
     * @param firstName
     *            first name of patient.
     * @param lastName
     *            last name of patient.
     * @param personalID
     *            Personal ID of patient.
     */
    public Patient(String firstName, String lastName, String personalID) {
        variableCorrect(firstName, lastName, personalID);
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = null;
        this.generalPractitioner = null;
        this.personalID = personalID;
    }

    /**
     * Second constructor of the class.
     *
     * @param firstName
     *            first name of patient.
     * @param lastName
     *            last name of patient.
     * @param generalPractitioner
     *            General practitioner of the patient.
     * @param personalID
     *            Personal ID of patient.
     */
    public Patient(String firstName, String lastName, String generalPractitioner, String personalID) {
        variableCorrect(firstName, lastName, personalID);
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = null;
        this.generalPractitioner = generalPractitioner;
        this.personalID = personalID;
    }

    /**
     * Third constructor of the class.
     *
     * @param firstName
     *            first name of patient.
     * @param lastName
     *            last name of patient.
     * @param diagnosis
     *            diagnosis of the patient.
     * @param generalPractitioner
     *            General practitioner of the patient.
     * @param personalID
     *            Personal ID of patient.
     */
    public Patient(String firstName, String lastName, String diagnosis, String generalPractitioner, String personalID) {
        variableCorrect(firstName, lastName, personalID);
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
        this.personalID = personalID;
    }

    /**
     *
     */
    public Patient() {
    }

    public void variableCorrect(String firstName, String lastName, String personalID) {
        if (firstName.isBlank() || lastName.isBlank()) {
            throw new IllegalArgumentException("First/Last name is blank.");
        }
        if (!(personalID.length() == 11) || !personalID.matches("[0-9]+")) {
            throw new IllegalArgumentException("Social security number not valid(Must be 11 digits).");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Patient patient = (Patient) o;
        return Objects.equals(personalID, patient.personalID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personalID);
    }

    @Override
    public String toString() {
        return "\nPatient; " + " first name: " + firstName + ", last name: " + lastName + ", diagnosis: " + diagnosis
                + ", general practitioner: " + generalPractitioner + ", personalID: " + personalID + ",";
    }
}
