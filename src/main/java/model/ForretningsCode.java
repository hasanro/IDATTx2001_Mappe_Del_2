package model;

import factory.PopUp;
import javafx.collections.ObservableList;
import Task3.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static controllers.PatientRegisterController.getPatient;

/**
 * Class to Read or Write patients to/from files, validate patient and set variables to patient. (Forretnings kode)
 */
public class ForretningsCode {

    /**
     * Checks if personalID already exists in for another patient in the register.
     */
    public static boolean validatePatient(String iDToValidate, ObservableList<Patient> register) {
        return register.stream().filter(p -> !p.equals(getPatient()))
                .anyMatch(p -> p.getPersonalID().equals(iDToValidate));
    }

    /**
     * Sets new variables for selected patient.
     *
     * @param updatePatient
     *            new patient object.
     */
    public static void updatePatientInRegister(Patient updatePatient, ObservableList<Patient> register) {
        register.stream().filter(p -> p.equals(getPatient())).forEach(p -> {
            p.setFirstName(updatePatient.getFirstName());
            p.setLastName(updatePatient.getLastName());
            p.setDiagnosis(updatePatient.getDiagnosis());
            p.setGeneralPractitioner(updatePatient.getGeneralPractitioner());
            p.setPersonalID(updatePatient.getPersonalID());
        });
    }

    /**
     * Reads through csv file and puts patient variables in a List String[]. Receives new patients and adds them to an
     * arraylist.
     *
     * @param cSVFile
     *            file to read.
     *
     * @return list of new patients.
     *
     * @throws IOException
     *             exception thrown if buffered reader not able to read file.
     */
    public static List<Patient> readData(File cSVFile) throws IOException {
        List<Patient> patients = new ArrayList<>();
        try (BufferedReader bReader = new BufferedReader(new FileReader(cSVFile))) {
            String line;
            while ((line = bReader.readLine()) != null) {
                String[] patientInfo = line.split(";"); // Sets variables of patient into String array
                Patient newPatient = readPatient(patientInfo); // new Patient set as Patient created in readPatient
                patients.add(newPatient);
            }
        } catch (FileNotFoundException e) {
            PopUp.exceptionDialog("File not read.", e.getMessage()).show();
        }
        return patients;
    }

    /**
     * Reads String array made by readData method and creates new Patients from them.
     *
     * @param info
     *            patient variables as String[].
     *
     * @return new patient.
     */
    public static Patient readPatient(String[] info) {
        String firstName = info[0];
        String lastName = info[1];
        String genPractitioner = info[2];
        String securityNumber = info[3].trim();
        return new Patient(firstName, lastName, genPractitioner, securityNumber);
    }

    /**
     * Writes a String list of patients and their variables divided by ; to the chosen file.
     *
     * @param file
     *            file to write patients into
     *
     * @throws IOException
     *             exception thrown
     */
    public static void writeToCSV(File file, ArrayList<Patient> listToExportFrom) throws IOException {
        BufferedWriter bWriter = new BufferedWriter(new FileWriter(file));
        for (int i = 0; i < listOfPatientsToCSV(listToExportFrom).size(); i++) {
            bWriter.write(listOfPatientsToCSV(listToExportFrom).get(i));
            bWriter.newLine();
        }
        bWriter.close();
    }

    /**
     * Puts patients from patient register in a list with variables divided by ;.
     *
     * @return List of patients.
     */
    public static List<String> listOfPatientsToCSV(ArrayList<Patient> listToExportFrom) {
        List<String> patients = new ArrayList<>();
        patients.add("First Name;Last NameDiagnosis;General Practitioner;Social Security Number");
        for (int i = 0; i < listToExportFrom.size(); i++) {
            patients.add(listToExportFrom.get(i).getFirstName() + ";" + listToExportFrom.get(i).getLastName() + ";"
                    + listToExportFrom.get(i).getDiagnosis() + ";" + listToExportFrom.get(i).getGeneralPractitioner()
                    + ";" + listToExportFrom.get(i).getPersonalID());
        }
        return patients;
    }
}
