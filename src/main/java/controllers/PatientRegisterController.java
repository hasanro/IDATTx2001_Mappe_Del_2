package controllers;

import App.App;
import daos.PatientDAO;
import factory.PopUp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import model.Patient;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import static model.ForretningsCode.readData;
import static model.ForretningsCode.writeToCSV;

/**
 * Controller class of patient-register fxml file.
 */
public class PatientRegisterController {

    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> generalPractitionerColumn;
    @FXML
    private TableColumn<Patient, String> personalIDColumn;
    @FXML
    private Label statusLabel;

    private static Label accessibleLabel;

    private static ObservableList<Patient> patientRegister;

    private static Patient patient;// Patient clicked on.

    /**
     * This method runs as fxml file window is loading.
     *
     * Fills up tableview with dummy patients and initializes observableList of patients.
     *
     */
    public void initialize() {
        patientRegister = FXCollections.observableArrayList();
        patientRegister.add(new Patient("Michael", "Corleone", "Vito Corleone", "12345678901"));
        patientRegister.add(new Patient("Ace", "Ventura", "Rhinoceros", "56789012345"));
        patientRegister.add(new Patient("Marty", "McFly", "Doc", "19851955012"));
        patientRegister.add(new Patient("Beatrix", "Kiddo", "Billy", "34567890123"));
        patientRegister.add(new Patient("Forrest ", "Gump", "Jenny", "45678901234"));
        patientRegister.add(new Patient("Tyler", "Durden", "Insane", "Tyler Durden", "01234567890"));

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("generalPractitioner"));
        personalIDColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("personalID"));

        tableView.setItems(patientRegister);// Sets tableview

        accessibleLabel = statusLabel;// Setting the static label = to status label to be able to update from other
                                      // class

    }

    /**
     * Sets status of the application.
     *
     * @param status
     *            status to be updated.
     */
    public static void setAccessibleLabel(String status) {
        accessibleLabel.setText(status);
    }

    public static Patient getPatient() {
        return patient;
    }

    public static void setPatient(Patient patient) {
        PatientRegisterController.patient = patient;
    }

    @FXML
    public static ObservableList<Patient> getPatientRegister() {
        return patientRegister;
    }

    /**
     * Method to load and show patient-details fxml file.
     * 
     * @throws IOException
     *             exception thrown if window not loading.
     */
    @FXML
    public void addPatient() throws IOException {
        Stage window = PopUp.getPopUp("patient-details");
        window.setTitle("Add patient");
        window.initModality(Modality.APPLICATION_MODAL);
        window.show();
    }

    /**
     * Method to load and show delete confirmation and delete selected patient if wanted.
     *
     */
    @FXML
    public void removePatient() {
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete patient? \n" + "\nFirst name:\n "
                + tableView.getSelectionModel().getSelectedItem().getFirstName() + "\n" + "\nLast name:\n "
                + tableView.getSelectionModel().getSelectedItem().getLastName() + "\n" + "\nSocial security number:\n "
                + tableView.getSelectionModel().getSelectedItem().getPersonalID(), ButtonType.YES, ButtonType.NO);
        alert.setTitle("Delete Patient");
        alert.setHeaderText("Delete confirmation");
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            tableView.getItems().removeAll(tableView.getSelectionModel().getSelectedItem());
            statusLabel.setText("Patient removed from register.");
        }
    }

    /**
     * Method to load and show patient-details fxml file with patient to edit.
     * 
     * @throws IOException
     *             exception thrown if window fails to load.
     */
    @FXML
    public void editPatient() throws IOException {
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        patient = tableView.getSelectionModel().getSelectedItem();// Sets patient variable in class to the one clicked.

        Stage window = PopUp.getPopUp("patient-details");
        window.setTitle("Edit patient");
        window.initModality(Modality.APPLICATION_MODAL);
        window.showAndWait();

        tableView.refresh();
        patient = null;// Sets patient back as null.
    }

    /**
     * Runs FileChooser with only csv file able to be chosen. Imports patients from csv file to patient register.
     *
     *
     */
    @FXML
    public void importCSV() {
        ObservableList<Window> stage = Stage.getWindows();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open file to import patients from");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File file = fileChooser.showOpenDialog(stage.get(0));
        try {
            if (file != null) {
                readData(file);
                patientRegister.addAll(readData(file));
                statusLabel.setText("Patients imported!");
            }
        } catch (Exception e) {
            PopUp.exceptionDialog("Import failed!", e.getMessage()).show();
        }

    }

    /**
     * Exports patient register to a chosen file.
     *
     */
    @FXML
    public void exportCSV() {
        ObservableList<Window> stage = Stage.getWindows();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file to export patients to");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File file = fileChooser.showSaveDialog(stage.get(0));
        ArrayList<Patient> patientsToExport = new ArrayList<>(patientRegister);
        try {
            writeToCSV(file, patientsToExport);
            statusLabel.setText("Patients exported!");
        } catch (Exception e) {
            PopUp.exceptionDialog("Export Failed", e.getMessage()).show();
        }
    }

    /**
     * Retrieves patients from database and adds them to patient register
     */
    @FXML
    public void loadDatabase() {
        PatientDAO patientDAO = new PatientDAO(App.getEntityManager());
        try {
            patientRegister.addAll(patientDAO.getAll());
            tableView.refresh();
            statusLabel.setText("Patients retrieved from database!");
        } catch (SQLException e) {
            PopUp.exceptionDialog("Database load Failed", e.getMessage()).show();
        }
    }

    /**
     * Saves patients from register to database
     */
    @FXML
    public void saveDatabase() {
        PatientDAO patientDAO = new PatientDAO(App.getEntityManager());
        patientRegister.forEach(p -> {
            try {
                patientDAO.create(p);
            } catch (SQLException e) {
                System.out.println(e.getMessage());// Using System.out since trying to show popup window with exception
                // with too many patients already in database, would crash my pc.
            }
        });
        statusLabel.setText("Patients saved to database!");
    }

    /**
     * Method to load and show info fxml file.
     *
     *
     * @throws IOException
     *             exception thrown if window fails to load.
     */
    public void help() throws IOException {
        Stage window = PopUp.getPopUp("info");
        window.show();
    }

    /**
     * Method closes the application.
     */
    @FXML
    public void exit() {
        System.exit(0);
    }

}
