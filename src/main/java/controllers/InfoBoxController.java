package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Controller class for info fxml file.
 */
public class InfoBoxController {

    @FXML
    public Button okButton;

    /**
     * Closes window when button is clicked.
     */
    public void okCool() {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }
}
