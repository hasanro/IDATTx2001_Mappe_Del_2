package controllers;

import factory.PopUp;
import model.Patient;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.ForretningsCode;

/**
 * Controller class for patient-details fxml file.
 */
public class AddEditController {

    @FXML
    public TextField firstNameField;
    @FXML
    public TextField lastNameField;
    @FXML
    public TextField diagnosisField;
    @FXML
    public TextField generalPractitionerField;
    @FXML
    public TextField personalIDField;
    @FXML
    public Button cancelButton;

    /**
     * Sets prompt text if add patient is clicked, and sets text if edit patient is clicked.
     */
    @FXML
    public void initialize() {
        if (PatientRegisterController.getPatient() != null) {
            firstNameField.setText(PatientRegisterController.getPatient().getFirstName());
            lastNameField.setText(PatientRegisterController.getPatient().getLastName());
            diagnosisField.setText(PatientRegisterController.getPatient().getDiagnosis());
            generalPractitionerField.setText(PatientRegisterController.getPatient().getGeneralPractitioner());
            personalIDField.setText(PatientRegisterController.getPatient().getPersonalID());
        } else {
            firstNameField.setPromptText("First name");
            lastNameField.setPromptText("Last name");
            diagnosisField.setPromptText("Diagnosis");
            // diagnosisField.setEditable(false); wanted to make diagnosis field non editable, to be edited separately,
            // but since we have a PersonalID input I changed my mind.
            generalPractitionerField.setPromptText("General practitioner");
            personalIDField.setPromptText("Social security number");
        }
    }

    /**
     * Adds patient to the patient register or edits a selected patient from the register. And closes window if
     * exception not thrown.
     *
     */
    @FXML
    public void confirm() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();

        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String diagnosis = diagnosisField.getText();
        String gP = generalPractitionerField.getText();
        String personalID = String.valueOf(personalIDField.getText());
        try {
            Patient patient = new Patient(firstName, lastName, diagnosis, gP, personalID);
            if (stage.getTitle().equals("Add patient")) {// checks if the window opened is Add patient
                if (PatientRegisterController.getPatientRegister().contains(patient)) {
                    throw new IllegalArgumentException("Social security number already belongs to another patient.");
                } else {
                    PatientRegisterController.getPatientRegister().add(patient);
                    PatientRegisterController.setAccessibleLabel("Patient added to register.");
                    stage.close();
                }
            } else if (stage.getTitle().equals("Edit patient")) {// checks if the window opened is Edit patient
                if (firstName.isBlank() || lastName.isBlank()) {
                    throw new IllegalArgumentException("First/Last name is blank.");
                }
                if (!(personalID.length() == 11) || !personalID.matches("[0-9]+")) {
                    throw new IllegalArgumentException("Social security number not valid(Must be 11 digits).");
                }
                if (ForretningsCode.validatePatient(personalID, PatientRegisterController.getPatientRegister())) {
                    throw new IllegalArgumentException(
                            "Social security number belongs to another patient \nin the register.");
                }
                ForretningsCode.updatePatientInRegister(patient, PatientRegisterController.getPatientRegister());
                PatientRegisterController.setAccessibleLabel("Patient edited.");
                stage.close();

            }
        } catch (Exception e) {
            PopUp.exceptionDialog("Confirmation failed", e.getMessage()).show();
        }
    }

    /**
     * Closes window.
     *
     */
    @FXML
    public void cancel() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
}
