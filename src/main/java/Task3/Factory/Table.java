package Task3.Factory;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Patient;

/**
 * Class to set a type of table with specific columns.
 */
public class Table {
    private String nameOfTable;
    private TableView<Patient> tableView = new TableView<>();

    private TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First Name");
    private TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last Name");
    private TableColumn<Patient, String> generalPractitionerColumn = new TableColumn<>("General Practitioner");
    private TableColumn<Patient, String> personalIDColumn = new TableColumn<>("Social Security Number");

    public Table(String whatKindOfTable) {

        if (whatKindOfTable.trim().equalsIgnoreCase("registerTable")) {
            this.tableView.getColumns().addAll(getFirstNameColumn(), getLastNameColumn(),
                    getGeneralPractitionerColumn(), getPersonalIDColumn());
            this.nameOfTable = whatKindOfTable;
        }
    }

    public TableColumn<Patient, String> getFirstNameColumn() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        return firstNameColumn;
    }

    public TableColumn<Patient, String> getLastNameColumn() {
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        return lastNameColumn;
    }

    public TableColumn<Patient, String> getGeneralPractitionerColumn() {
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        return generalPractitionerColumn;
    }

    public TableColumn<Patient, String> getPersonalIDColumn() {
        personalIDColumn.setCellValueFactory(new PropertyValueFactory<>("personalID"));
        return personalIDColumn;
    }

    public String getNameOfTable() {
        return nameOfTable;
    }
}
