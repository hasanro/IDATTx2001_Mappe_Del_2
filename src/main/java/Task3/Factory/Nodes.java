package Task3.Factory;

/**
 * Interface class for the nodes of the window.
 */
public interface Nodes {
    MenuNodes getMenu();

    Table getTable();

    void setMenu(MenuNodes menuNodes);

    void setTable(Table table);

}
