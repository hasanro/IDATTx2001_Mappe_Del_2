package Task3.Factory;

/**
 * This class will set the window.
 */
public class RegisterWindow implements Nodes {
    private Table table;
    private MenuNodes menuNodes;

    public RegisterWindow(Table table, MenuNodes menuNodes) {
        this.table = table;
        this.menuNodes = menuNodes;
    }

    @Override
    public void setMenu(MenuNodes menuNodes) {
        this.menuNodes = menuNodes;
    }

    @Override
    public MenuNodes getMenu() {
        return menuNodes;
    }

    @Override
    public void setTable(Table table) {
        this.table = table;
    }

    @Override
    public Table getTable() {
        return table;
    }
}
