package Task3.Factory;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * Class to set a type of menu with specific menu items and names.
 */
public class MenuNodes {
    private String nameOfTable;

    private Menu fileMenu = new Menu("File");
    private Menu editMenu = new Menu("Edit");
    private Menu helpMenu = new Menu("Help");
    private MenuBar menuBar = new MenuBar();

    public MenuNodes(String typeOfMenu) {
        if (typeOfMenu.trim().equalsIgnoreCase("mainMenu")) {
            this.menuBar.getMenus().addAll(getFileMenu(), getEditMenu(), getHelpMenu());
            this.nameOfTable = typeOfMenu;
        }
    }

    public Menu getFileMenu() {
        fileMenu.getItems().add(new MenuItem("Import from .csv"));
        fileMenu.getItems().add(new MenuItem("Export to csv"));
        fileMenu.getItems().add(new MenuItem("Exit"));
        return fileMenu;
    }

    public Menu getEditMenu() {
        editMenu.getItems().add(new MenuItem("Add patient"));
        editMenu.getItems().add(new MenuItem("Remove patient"));
        editMenu.getItems().add(new MenuItem("Edit patient"));
        return editMenu;
    }

    public Menu getHelpMenu() {
        helpMenu.getItems().add(new MenuItem("About"));
        return helpMenu;
    }

    public String getNameOfTable() {
        return nameOfTable;
    }
}
