package Task3;

import Task3.Factory.Table;
import Task3.Factory.Nodes;
import Task3.Factory.MenuNodes;
import Task3.Factory.RegisterWindow;
import javafx.stage.Stage;

/**
 * Window factory to get desired window.
 */
public class WindowFactory {
    public Nodes getDesiredWindow(Table mainTable, MenuNodes menuNodes) {
        if (mainTable.getNameOfTable().trim().equalsIgnoreCase("registerTable")
                && menuNodes.getNameOfTable().trim().equalsIgnoreCase("mainMenu")) {
            return new RegisterWindow(mainTable, menuNodes);
        }
        return null;
    }
}

/**
 * Example on how one would call and make a window.
 */
class example {
    Stage stage = new Stage();// from App start class
    WindowFactory windowFactory = new WindowFactory();

    MenuNodes menuNodes = new MenuNodes("Main Menu");
    Table table = new Table("Register Table");

    Nodes mainWindow = windowFactory.getDesiredWindow(table, menuNodes);

}
