package App;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;

import static factory.FXMLFile.loadFXML;

/**
 * Class to start the app.
 */
public class App extends Application {
    private static Scene scene;

    private static final EntityManagerFactory entityFactory = Persistence
            .createEntityManagerFactory("st-olavs-register");
    private static final EntityManager entityManager = entityFactory.createEntityManager();

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Launches the FX app.
     * 
     * @param stage
     *            stage from JavaFx.
     * 
     * @throws IOException
     *             problem with launch of app.
     */
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("patient-register"));
        stage.setScene(scene);
        stage.setTitle("Patient Register");
        stage.getIcons().add(new Image(String.valueOf(this.getClass().getResource("/images/register.png"))));
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
